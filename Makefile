TARGET = fibonacci
OUT_FORMAT = pdf
# eqn and iconv
PREPROCESSORS = -ek
# Unsafe mode must be set to use file writing macros.
SAFETY = -U
SOURCE = $(TARGET).ms
OUTPUT = $(SOURCE:.ms=.$(OUT_FORMAT))

all: $(OUTPUT) $(TARGET).elf

# It is necessary to soelim the source file to make use of the lf hijack.
$(OUTPUT): lit.tmac $(SOURCE)
	soelim $(SOURCE) | groff -T$(OUT_FORMAT) $(SAFETY) $(PREPROCESSORS) -ms > $@

view: $(TARGET).$(OUT_FORMAT)
	setsid zathura $< &

clean:
	$(RM) $(OUTPUT) *.c *.h *.o $(TARGET).elf

$(TARGET).elf: $(OUTPUT)
	$(CC) -c $(TARGET).c
	$(CC) -c main.c
	cc -o $@ *.o
	
