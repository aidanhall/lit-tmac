# lit.tmac
* This file defines **g**troff macros for literate programming.
* It is currently a poor imitation of CWEB: http://www.literateprogramming.com/cweb_download.html
* The user can redefine some macros to enhance its behaviour:
    * `l<` and `l>` allow the user to control how listings of code are rendered.
    * `l*` allows the user to insert information such as `#line` directives into code blocks, allowing compiler messages to point to the place in the original (gtroff) file where that code exists.
* It does not depend on any of the standard macro packages, but can be enriched by them.
* Tested and seems to work alongside the m and s macro sets.
## Usage
The `Makefile` and `fibonacci.ms` represent an example of how this may be used.
## Limitations
* It is currently single-pass, so it is incapable of more than an illusion of non-linear cross-references.
* Most of the macros are 2 characters, a troff limitation I have abided by despite my use of numerous groff extensions, one of which is longer identifiers. Consequently, it is cryptic and I doubt anyone but me could use it if they tried.
* Code can only be written to files if groff is run in unsafe mode (`-U`), since it calls unsafe macros such as `.write`.
* Backslashes must be doubled, since troff uses them as an escape character:
```c
printf("Hello world.\\n");
```
