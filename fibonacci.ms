.fam P
.so lit.tmac
.EQ
delim on
delim $$
.EN
.de l<
.DS I
.CW
.nm 1 1
..
.de l>
.nm
.DE
..
.de l*
#line \\$1 "\\$2"
..
.TL
Literate Fibonacci Program.
.AU
Aidan Hall
.NH 1
Fibonacci Program
.PP
This is a literate Fibonacci program created using my
.CW
lit.tmac
.R
macro set for groff.
Here is the program:
.l^ fibonacci.c fibonacci Main Fibonacci Program
#include "fibonacci.h"
.@*; recursive-fib 0 ;
.@*; iterative-fib 0 ;
.l-
.NH 1
Recursive Fibonacci.
.PP
The classic implementation of the 
Fibonacci
function is recursive.
The \f[C]fibonacci()\f[]
procedure calls itself recursively, using the equation for Fibonacci numbers:
.EQ
fibonacci(n) = fibonacci(n-1) + fibonacci(n-2)
.EN
It will recur until it reaches $n <= 1$.
.l= recursive-fib Recursive Fibonacci Algorithm
uint32_t fibonacci(uint32_t n)
{
	/* lolol */
	if (n <= 1) {
.		@*; base_case 2 ;
	} else {
.		@*; recursive_case 2 ;
	}
}
.l-
.NH 2
Recursive Case.
.PP
If a known base_case is not yet reached, continue calling recursively.
.l= recursive_case Continued Recursion Procedure
return fibonacci(n-1) + fibonacci(n-2);
.l-
.NH 2
Base Case.
.PP
For 0 and 1,
$fibonacci(n) = n$,
so the number itself can be returned.
.l= base_case Recursion Base/Terminating Case
return n;
.l-
.
.NH 1
Iterative Fibonacci
.LP
The recursive method is highly inefficient, so it is better to use an iterative approach:
.l= iterative-fib Iterative Fibonacci Algorithm
uint32_t fibonacci_iterative(uint32_t n) {
	/* At each iteration, we only need the last 2. */
	uint32_t fibs[2] = {0, 1};
	for (uint32_t i = 0; i < n; ++i) {
		/* fibs[i] = fibs[i-2] + fibs[i-1] */
		fibs[i%2] = fibs[(i-2)%2] + fibs[(i-1)%2];
	}
	/* The fibonacci number for n will be odd if it is, and vice versa. */
	return fibs[n%2];
}
.l-
.NH 1
Headers
.LP
A project may require multiple headers.
It would probably be sensible to have 1 web file correspond to just 1
\.c+.h pair.
The header file must include
.CW "stdint.h"
to allow me to use
.CW "uint32_t"
for the integers.
.l^ fibonacci.h fibonacci_h Fibonacci Header
#pragma once
#include <stdint.h>
.l-
.NH 2
Declarations
.PP
A header must contain declarations for functions, so they can be called
from other translation units.
.l+ fibonacci_h
uint32_t fibonacci(uint32_t n);
uint32_t fibonacci_iterative(uint32_t n);
.l-
.NH 1
Main Program
.PP
I want there to be a main program so I can run and test the
.CW "fibonacci"
functions.
.l^ main.c mainprog
#include "fibonacci.h"
#include <stdio.h>
#include <assert.h>
int main(int argc, char* argv[]) {
	for (int i = 0; i < 10; ++i) {
		assert(fibonacci(i) == fibonacci_iterative(i));
		printf("fib(%d):%d\\n\\
fib_iter(%d): %d\\n",
				i,
				fibonacci(i),
				i,
				fibonacci_iterative(i)
		      );
	}
}
.l-
